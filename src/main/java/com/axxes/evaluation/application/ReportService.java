package com.axxes.evaluation.application;

import com.axxes.evaluation.domain.Consultant;
import com.axxes.evaluation.domain.ConsultantRepository;
import com.axxes.evaluation.domain.Report;
import com.axxes.evaluation.domain.ReportRepository;
import com.axxes.evaluation.infrastructure.ReportDTO;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.stream.Collectors;

@Service
public class ReportService {

    private final ReportRepository reportRepository;
    private final ConsultantRepository consultantRepository;

    @Autowired
    public ReportService(final ReportRepository reportRepository,
                         final ConsultantRepository consultantRepository) {
        this.reportRepository = reportRepository;
        this.consultantRepository = consultantRepository;

        reportRepository.init();
        consultantRepository.init();
    }

    public List<ReportDTO> findAll() {
        return reportRepository.findAll()
                .stream()
                .map(this::convertReportToDTO)
                .collect(Collectors.toList());
    }

    private ReportDTO convertReportToDTO(final Report report) {
        final Consultant consultant = consultantRepository.findById(report.getAuthorId());
        return new ReportDTO(report.getId(), report.getName(), consultant.fullName(), report.getText());
    }

    public ReportDTO findById(final String reportId) {
        final Report report = reportRepository.findById(reportId);
        return convertReportToDTO(report);
    }

    public ReportDTO save(final ReportDTO report) {
        final String consultantId = consultantRepository.findIdByFullName(report.getAuthor());
        final Report reportToSave = new Report(report.getId(), report.getName(), consultantId, report.getText());
        final Report savedReport = reportRepository.save(reportToSave);
        return convertReportToDTO(savedReport);
    }

    public void delete(String reportId) {
        reportRepository.delete(reportId);
    }
}
