package com.axxes.evaluation.infrastructure;

import com.axxes.evaluation.domain.Consultant;
import com.axxes.evaluation.domain.ConsultantRepository;
import org.springframework.stereotype.Repository;

import java.util.ArrayList;
import java.util.List;

@Repository
public class ConsultantRepositoryImpl implements ConsultantRepository {

    private final List<Consultant> consultants = new ArrayList<>();

    public void init() {
        consultants.add(new Consultant("consultant-id", "Thomas", "Van Winckel"));
    }

    @Override
    public Consultant findById(String consultantId) {
        for (final Consultant c : consultants) {
            if (c.getId().equals(consultantId)) return c;
        }
        throw new ConsultantNotFoundException();
    }


    @Override
    public String findIdByFullName(final String fullName) {
        for (final Consultant c : consultants) {
            if (c.fullName().equals(fullName)) return c.getId();
        }
        throw new ConsultantNotFoundException();
    }

    public class ConsultantNotFoundException extends RuntimeException {
    }
}
