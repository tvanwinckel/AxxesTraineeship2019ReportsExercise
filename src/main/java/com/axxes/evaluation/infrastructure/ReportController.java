package com.axxes.evaluation.infrastructure;

import com.axxes.evaluation.application.ReportService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.servlet.ModelAndView;

import java.util.List;

@Controller
@RequestMapping(path = "/reports")
public class ReportController {

    private final ReportService reportService;

    @Autowired
    public ReportController(final ReportService reportService) {
        this.reportService = reportService;
    }

    @GetMapping
    public ModelAndView getAllReports() {
        final List<ReportDTO> reports = reportService.findAll();

        final ModelAndView view = new ModelAndView("reports-overview");
        view.addObject("reports", reports);
        return view;
    }

    @GetMapping("/{id}")
    public ModelAndView getReport(@PathVariable("id") final String reportId) {
        final ReportDTO report = reportService.findById(reportId);

        final ModelAndView view = new ModelAndView("report");
        view.addObject("report", report);
        return view;
    }

    @PostMapping("/{id}")
    public ModelAndView save(@ModelAttribute final ReportDTO report) {
        final ReportDTO savedReport = reportService.save(report);

        final ModelAndView view = new ModelAndView("report");
        view.addObject("report", savedReport);
        return view;
    }

    @DeleteMapping("/{id}")
    public ModelAndView delete(@PathVariable("id") final String reportId) {
        reportService.delete(reportId);
        return getAllReports();
    }
}
