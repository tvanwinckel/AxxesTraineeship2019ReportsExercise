package com.axxes.evaluation.infrastructure;

import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.ControllerAdvice;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.ResponseStatus;
import org.springframework.web.servlet.ModelAndView;

@ControllerAdvice(basePackages = "com.axxes.evaluation")
public class ExceptionAdvice {

    @ExceptionHandler(ConsultantRepositoryImpl.ConsultantNotFoundException.class)
    @ResponseStatus(HttpStatus.NOT_FOUND)
    public ModelAndView consultantNotFound(final Exception e) {
        final ModelAndView view = new ModelAndView("oopsView");
        view.addObject("message", "Something went wrong, given consultant was not found.");
        return view;
    }


    @ExceptionHandler(ReportRepositoryImpl.ReportNotFoundException.class)
    @ResponseStatus(HttpStatus.NOT_FOUND)
    public ModelAndView reportNotFound(final Exception e) {
        final ModelAndView view = new ModelAndView("oopsView");
        view.addObject("message", "Could not find given report.");
        return view;
    }
}
