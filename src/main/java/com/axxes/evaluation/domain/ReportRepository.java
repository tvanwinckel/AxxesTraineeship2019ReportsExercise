package com.axxes.evaluation.domain;

import java.util.List;

public interface ReportRepository {

    void init();

    List<Report> findAll();

    Report findById(String reportId);

    Report save(Report report);

    void delete(String reportId);
}
