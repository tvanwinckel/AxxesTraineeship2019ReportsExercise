package com.axxes.evaluation.infrastructure;

import com.axxes.evaluation.domain.Report;
import com.axxes.evaluation.domain.ReportRepository;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Repository;

import java.util.ArrayList;
import java.util.List;

@Repository
public class ReportRepositoryImpl implements ReportRepository {

    private final Logger LOGGER = LoggerFactory.getLogger(ReportRepositoryImpl.class);
    private final List<Report> reports = new ArrayList<>();

    public void init() {
        final Report report1 = new Report("12-ab-34", "Example report 1", "consultant-id", "This is an example reports summary");
        final Report report2 = new Report("34-cd-45", "Example report 2", "consultant-id", "This is an example reports summary");
        final Report report3 = new Report("45-ef-67", "Example report 3", "consultant-id", "This is an example reports summary");
        reports.add(report1);
        reports.add(report2);
        reports.add(report3);
    }

    @Override
    public List<Report> findAll() {
        return reports;
    }

    @Override
    public Report findById(final String reportId) {
        for (final Report report : reports) {
            if (report.getId().equals(reportId)) return report;
        }
        throw new ReportNotFoundException();
    }

    @Override
    public Report save(final Report report) {
        return reports.contains(report)
                ? updateReport(report)
                : addReport(report);
    }

    @Override
    public void delete(String reportId) {
        reports.removeIf(r -> r.getId().equals(reportId));
    }

    private Report addReport(final Report report) {
        LOGGER.info("Saving new report with id: {}", report.getId());
        reports.add(report);
        return report;
    }

    private Report updateReport(final Report report) {
        LOGGER.info("Updating report with id: {}", report.getId());
        reports.remove(report);
        reports.add(report);
        return report;
    }

    public class ReportNotFoundException extends RuntimeException {
    }
}
