package com.axxes.evaluation.domain;

public interface ConsultantRepository {

    void init();

    Consultant findById(String consultantId);

    String findIdByFullName(String fullName);
}
