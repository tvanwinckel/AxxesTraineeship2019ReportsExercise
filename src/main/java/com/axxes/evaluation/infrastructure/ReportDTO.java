package com.axxes.evaluation.infrastructure;

public class ReportDTO {

    private final String id;
    private final String name;
    private final String author;
    private final String text;

    public ReportDTO(final String id, final String name, final String author, final String text) {
        this.id = id;
        this.name = name;
        this.author = author;
        this.text = text;
    }

    public String getId() {
        return id;
    }

    public String getName() {
        return name;
    }

    public String getAuthor() {
        return author;
    }

    public String getText() {
        return text;
    }
}
